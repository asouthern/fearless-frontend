import React, { useEffect, useState} from 'react';

function ConferenceForm (props){
    const [locations, setLocations] = useState([])
    const [name, setName] = useState('');
    const [startDate, setStartDate] = useState('')
    const [endDate, setEndDate] = useState('')
    const [description, setDescription] = useState('')
    const [maxPresentations, setMaxPresentations] = useState('')
    const [maxAttendees, setMaxAttendees] = useState('')
    const [location, setLocation] = useState([])




    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value)
    }

    const handleStartDate = (event) => {
        const value = event.target.value;
        setStartDate(value)
    }

    const handleEndDate = (event) => {
        const value = event.target.value;
        setEndDate(value)
    }

    const handleDescription = (event) => {
        const value = event.target.value;
        setDescription(value)
    }

    const handleMaxPresentations = (event) => {
        const value = event.target.value;
        setMaxPresentations(value)
    }

    const handleMaxAttendees = (event) => {
        const value = event.target.value;
        setMaxAttendees(value)
    }

    const handleLocation = (event) => {
        const value = event.target.value;
        setLocation(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
      
        const data = {};
        
        data.name = name;
        data.starts = startDate;
        data.ends = endDate;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location

       

        
        
        
        
      
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
      
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
          const newConference = await response.json();
          
          setName('');
          setStartDate('')
          setEndDate('')
          setDescription('')
          setMaxPresentations('')
          setMaxAttendees('')
          setLocation('')
          
        } else {
            console.log('did not post')
            
            
        }
      }




    const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';
    
    const response = await fetch(url);
    
        if (response.ok) {
          const data = await response.json();
          
          setLocations(data.locations)
    
          
        }
      }
    
      useEffect(() => {
        fetchData();
      }, []);

    return (
    
      <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new conference</h1>
              <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Name" required
                type="text" value={name} name="name" id="name" className="form-control" />
                  <label htmlFor="name">Conference Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleStartDate} placeholder="Starts" required type="date" value={startDate} name='starts' id="starts" className="form-control"/>
                    <label htmlFor="starts">Start Date</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={handleEndDate} placeholder="Ends" required type="date" value={endDate} name='ends' id="ends" className="form-control"/>
                    <label htmlFor="ends">End Date</label>
                  </div>



                  <div className="mb-3">
                    
                    
                    <textarea onChange={handleDescription} className="form-control"  name='description' value={description} id="description" rows="3"></textarea>
                    <label htmlFor="description">Description</label>
                  </div>



                  <div className="form-floating mb-3">
                    <input onChange={handleMaxPresentations} placeholder="max_presentations" required type="number" name='max_presentations' value={maxPresentations} id="max_presentations" className="form-control"/>
                    <label htmlFor="max_presentations">Max Presentations</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={handleMaxAttendees} placeholder="max_attendees" required type="number" name='max_attendees' value={maxAttendees} id="max_attendees" className="form-control"/>
                    <label htmlFor="max_attendees">Max Attendees</label>
                  </div>


                  <div className="mb-3">
                    <select onChange={handleLocation} required id="location"value={location} name='location' className="form-select">
                      <option value="">Choose a location</option>
                      {locations.map(location =>{
                        return (
                            <option key={location.id} value={location.id}>
                                {location.name}
                            </option>
                        )
                      })}
                    </select>
                  </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
        
      );


}

export default ConferenceForm;