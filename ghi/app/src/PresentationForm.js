import React, {useEffect, useState } from 'react';

function PresentationForm(){
    const [conferences, setConferences] = useState([]);
    const [presenterName, setPresenterName] = useState('');
    const [presenterEmail, setPresenterEmail] = useState('');
    const [companyName, setCompanyName] = useState('')
    const [title, setTitle] = useState('')
    const [synopsis, setSynopsis] = useState('')

    const [conference, setConference] = useState('');

    

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          console.log(data)
          setConferences(data.conferences);
          
        }
      }
    
      useEffect(() => {
        fetchData();
      }, []);

    const handleChangeConference = (event) => {
        const value = event.target.value;
        setConference(value);
      }

    const handleChangePresenterName = (event) => {
        const value = event.target.value;
        setPresenterName(value);
      }

    const handleChangePresenterEmail = (event) => {
        const value = event.target.value;
        setPresenterEmail(value);
      }

    const handleChangeCompanyName = (event) => {
        const value = event.target.value;
        setCompanyName(value)
    }

    const handleTitleChange = (event) => {
        const value = event.target.value
        setTitle(value)
    }

    const handleChangeSynopsis = (event) => {
        const value = event.target.value
        setSynopsis(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
      
        const data = {};
        
        data.presenter_name = presenterName
        data.company_name = companyName
        data.presenter_email = presenterEmail
        data.title = title
        data.synopsis = synopsis
        data.conference = conference
        
        
        
        const presentationUrl = `http://localhost:8000/${conference}presentations/`;
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
      
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
          const newPresentation = await response.json();
          
          setPresenterName('')
          setCompanyName('')
          setPresenterEmail('')
          setTitle('')
          setSynopsis('')
          setConference('')
          
        } else {
            console.log('did not post')
            console.log(data)
            
            
        }
      }

    return (
        <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={handleChangePresenterName}placeholder="Presenter name" required type="text" name="presenter_name" value={presenterName} id="presenter_name" className="form-control"/>
                <label for="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleChangePresenterEmail} placeholder="Presenter email" required type="email" name="presenter_email"value={presenterEmail} id="presenter_email" className="form-control"/>
                <label for="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleChangeCompanyName} placeholder="Company name" type="text" name="company_name" id="company_name" value={companyName} className="form-control"/>
                <label for="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleTitleChange} placeholder="Title" required type="text" name="title" id="title" value={title} className="form-control"/>
                <label for="title">Title</label>
              </div>
              <div className="mb-3">
                <label  for="synopsis">Synopsis</label>
                <textarea onChange={handleChangeSynopsis} className="form-control" id="synopsis" value={synopsis} rows="3" name="synopsis"></textarea>
              </div>
              <div className="mb-3">
                <select onChange={handleChangeConference} name="conference" value={conference}id="conference" className="form-select">
                <option value="">Choose a conference</option>
                    {conferences.map(conference => {
                      return (
                        <option key={conference.href} value={conference.href}>{conference.name}</option>
                      )
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    )
}

export default PresentationForm