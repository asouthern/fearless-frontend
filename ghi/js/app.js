


function createCard(name, description, pictureUrl, start, end, location) {
  return `
  
     <div class="shadow-sm p-3 mb-5 bg-white rounded">
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
          <div class="card-footer text-muted">
          ${start} - ${end}
        </div>

       
     
    </div>
    </div>
    `;
}

window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
      alert('Bad response!', primary)
    } else {
      const data = await response.json();
      console.log(data);

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          console.log(details);
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const starta = details.conference.starts;
          const start = new Date(starta)
            .toISOString()
            .replace(/T.*/, "")
            ;
          const location = details.conference.location.name;

          const enda = details.conference.ends;
          const end = new Date(enda)
            .toISOString()
            .replace(/T.*/, "");
          const html = createCard(
            title,
            description,
            pictureUrl,
            start,
            end,
            location
          );
          const column = document.querySelector(".row");
          column.innerHTML += html;
        }
      }
    }
  } catch (e) {
    
    alert('Error is raised', primary)
  }
});


var alertPlaceholder = document.getElementById('liveAlertPlaceholder')
var alertTrigger = document.getElementById('liveAlertBtn')

function alert(message, type) {
  var wrapper = document.createElement('div')
  wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'

  alertPlaceholder.append(wrapper)
}

if (alertTrigger) {
  alertTrigger.addEventListener('click', function () {
    alert('Nice, you triggered this alert message!', 'success')
  })
}