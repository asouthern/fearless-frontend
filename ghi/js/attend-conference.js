

window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
  
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
  
      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
      const hide = document.getElementById('conference').classList 
      hide.remove('d-none')
      const add = document.getElementById('loading-conference-spinner').classList
      add.add('d-none')
    }
  
  });




window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:3000/attend-conference.html'
    const response = await fetch(url)

    const formTag = document.getElementById('create-attendee-form')
    formTag.addEventListener('submit' , async event =>{
        event.preventDefault()

        const formData = new FormData(formTag)
        const json = JSON.stringify(Object.fromEntries(formData))

        const locationUrl = 'http://localhost:8001/api/attendees/'
        const fetchConfig = {
            method: 'post',
            body: json,
            headers: {
                'Content-Type': 'application/json'
            },
        }
        const response = await fetch(locationUrl, fetchConfig)
        if (response.ok){
            formTag.reset();
            const newAttendee = await response.json()
            const hide = document.getElementById('success-message').classList 
            hide.remove('d-none')
        }
    })


    
})